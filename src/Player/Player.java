/**
 *
 * @author Gloria Salas Paucar; netid: gjs143
 * @author Sandhya Veludandi ; netid: sv549
 */

package player;

public class Player {
	private boolean isWhite; 
	
	public Player(boolean isWhite) {
		this.isWhite = isWhite; 
	}
	
	public boolean isWhite() {
		return this.isWhite; 
	}
} 
  

  
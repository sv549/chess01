//@ Gloria Salas netid: gjs143
//@ Sandhya Veludandi ; netid: sv549


package chess01;
import java.util.Scanner;
import board.*;
import pieces.*; 

public class Chess {
	private static Board chess; 
	private static boolean whiteTurn; //boolean to check whose turn it is
	private static boolean prevMoveLegal; 
	private static boolean gameOver; //boolean to check if final move is finished
	private static boolean drawRequested; 
	
	//game ends with winner
	public static void gameEnds() {
		gameOver = true; 
		if(whiteTurn) System.out.println("White wins"); 
		else System.out.println("Black wins"); 
	}
	
	//game ends in draw
	public static void draw() {
		gameOver = true; 
		System.out.println("\ndraw");
	}
	
	public static void checkMove(String move) {
		
		//player resigned
		if(move.equals("resign")) {
			whiteTurn = !whiteTurn; 
			gameEnds(); 
			return; 
		}
		
	    //draw offered and accepted 
		if(move.equals("draw") && drawRequested) {
			draw(); 
			return; 
		}
		
		//check if the player's move is valid and if true move the chess piece
		prevMoveLegal = (chess.move(move, whiteTurn)) ? true : false; 
		if(prevMoveLegal) {
			whiteTurn = !whiteTurn; //switch turns with player
			
			//check if draw requested
			drawRequested = (move.contains("draw?")) ? true : false;
			
			//detect check and checkmate (code not written yet) 
			//if checkmate -> call gameEnds()
			
		}
		else { 
			System.out.println("\nIllegal move, try again");
		}

	}
	
	public static void playGame() {
		System.out.println("Playing game"); 
		chess = new Board(); 
		
		whiteTurn = true; 
		prevMoveLegal = true; 
		gameOver = false; //boolean to check if final move is finished
		drawRequested = false; 
		//get player input
		Scanner in = new Scanner(System.in);
		
		//while 
		while(!gameOver) {
			//if previous move was legal, other player's turn so print board
			if(prevMoveLegal) chess.printBoard();
			//prompt for player's move
			if(whiteTurn) System.out.println("White's move: "); 
			else System.out.println("Black's move: "); 
			String move = in.nextLine();
			checkMove(move); 
		}
		
		in.close(); 
	}
	
	public static void main(String args[]) {
		playGame(); 
	}
}

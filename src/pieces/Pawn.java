/**
 * Pawn chess piece, subclass of pieces
 *
 * @author Gloria Salas Paucar; netid: gjs143
 * @author Sandhya Veludandi ; netid: sv549
 */

package pieces;

import board.*;

//import board
public class Pawn implements Pieces {
	
	private char color;
	private char piece = 'p'; 
	private char promotion; 
	//tracks if last move made was 2 spaces 
	public boolean twiceJump;
	public boolean initialMove;
	public char enpassant;

	public Pawn(char c) {
		this.color = c; 
		initialMove = true;
		twiceJump = false;
		
	}
	
	public char getColor() {
		return this.color; 
	}
	
	public void setPromotionChar(char promotion) {
		this.promotion = promotion; 
	}
	
	public boolean isLegalMove(Tile start, Tile end, Tile[][] chessBoard) {
		
//		Rows corresponds to Y
//		Cols corresponds to X
//		row change -> vertical change
//		col change -> horizontal change
		
		//end tile's piece is not the same team as start tile's piece
		if(end.getPiece() != null && start.getPiece().getColor() == end.getPiece().getColor()) return false; 
		
		//moving horizontally means the columns change
		int horizontalMoves = Math.abs(start.getC() - end.getC()); 
		//moving vertically means the rows change
		int verticalMoves = Math.abs(start.getR() - end.getR()); 
				
		
		// white piece moves up (start > end) 
		if(start.getPiece().getColor() == 'w')
			verticalMoves = start.getR() - end.getR();
		
		//black piece moves down (end > start) 
		else 
			verticalMoves = end.getR() - start.getR();
	
		// try to see if it moves up or down
		// check to see if space is empty
		if(horizontalMoves == 0) {
			//moves up/down 1 space
			if(verticalMoves == 1) {
				initialMove = false;  
				//Check for promotion
		        if(start.getPiece().getColor() == 'w' && end.getR() == 0 ||
		           start.getPiece().getColor() == 'b' && end.getR() == 7) {
		        	//create new Queen (default) 
		            chessBoard[end.getR()][end.getC()].setPiece(promotion());
		            enpassant = '0';
                    return true;
		        }
		        return true; 
			}
		}
			  
		//move up/down 2 spaces, checking for the first move
		if(verticalMoves == 2 && initialMove == true) {
			// Check for the path is clear
			int middleR = (start.getR() + end.getR()) / 2;
            if(chessBoard[middleR][start.getC()].getPiece() == null) {
                 initialMove = false;
                 twiceJump = true;
                 enpassant = '0';
                 return true;
            }
        }
		
     //Diagonal capture
     if(horizontalMoves == 1 && verticalMoves == 1) {
        //Check for promotion
        if(start.getPiece().getColor() == 'w' && end.getR() == 0 ||
           start.getPiece().getColor() == 'b' && end.getR() == 7) {
        	//create new Queen (default) 
        	chessBoard[end.getR()][end.getC()].setPiece(promotion());

             enpassant = '0';
             return true;
         }

         //En passant
         Tile adjTile = chessBoard[start.getR()][end.getC()];
         if(adjTile.getPiece() != null && adjTile.getPiece().getColor() != start.getPiece().getColor() && 
            adjTile.getPiece() instanceof Pawn &&
                 ((Pawn) adjTile.getPiece()).twiceJump) {

             enpassant = '1';
             return true;
         }
         return true; 
     }


       enpassant = '0';
		
		
		return false; 
	}
	
	public Pieces promotion() {
        switch(promotion) {
            case 'R': return new Rook(this.getColor());
            case 'N': return new Knight(this.getColor());
            case 'B': return new Bishop(this.getColor());
            default: return new Queen(this.getColor());
        }
    }
	
	public String toString() {
		return "" + this.color + this.piece; 
	}
}


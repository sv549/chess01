/**
 * King chess piece, subclass of pieces
 *
 * @author Gloria Salas Paucar; netid: gjs143
 * @author Sandhya Veludandi ; netid: sv549
 */

package pieces;

import board.Tile;

public class King implements Pieces {
	
	private char color;
	private char piece = 'K'; 
	public boolean twiceJump;
	public boolean initialMove;
	public char castling;

	public King(char c) {
		this.color = c; 
		initialMove = true;
		twiceJump = false;
		castling = '0'; 
	}
	
	public char getColor() {
		return this.color; 
	}

	public boolean isLegalMove(Tile start, Tile end, Tile[][] chessBoard) {
		
//		Rows corresponds to Y
//		Cols corresponds to X
//		row change -> vertical change
//		col change -> horizontal change
		
		//end tile's piece is not the same team as start tile's piece
		if(end.getPiece() != null && start.getPiece().getColor() == end.getPiece().getColor()) return false; 
		//moving horizontally means the columns change
		int horizontalMoves = Math.abs(start.getC() - end.getC()); 
		//moving vertically means the rows change
		int verticalMoves = Math.abs(start.getR() - end.getR()); 
		
		//a move to an adjacent square
		if((horizontalMoves == 1 && verticalMoves == 0) ||
		           (horizontalMoves == 0 && verticalMoves == 1) ||
		           (horizontalMoves == 1 && verticalMoves == 1)) {
		           		initialMove = false;
		           	    castling = '0';
		           		return true;
		}


		//Castling
		if(horizontalMoves == 2 && verticalMoves == 0 && initialMove) {
				Rook leftRook = (Rook) chessBoard[start.getR()][0].getPiece();
		        Rook rightRook = (Rook) chessBoard[start.getR()][7].getPiece();

		        //Moving left
		        if(start.getC() > end.getC()) {
		            if(leftRook != null &&
		                leftRook.initialMove &&
		                start.getPiece().getColor() == leftRook.getColor() ){

		                //Make sure path is clear
		            	for(int i = start.getC() - 1; i > 0; i--) 
		                     if( chessBoard[start.getR()][i].getPiece() != null) return false;
		            		 
		            	initialMove = false;
		            	castling = 'l'; 
		            	return true; 
		            }
		        }
		        
		        else {
		        	System.out.println("Right castling"); 
		        	if(rightRook != null &&
		                rightRook.initialMove &&
		                start.getPiece().getColor() == rightRook.getColor()){

		                //Make sure path is clear
		                for(int j = start.getC() + 1; j < 7; j++)
		                    if(chessBoard[start.getR()][j].getPiece() != null) return false;
		                initialMove = false;
		                castling = 'r';
		                return true;
		            }
		       }
		}
		            
		return false; 
			
	}      
	public String toString() {
		return "" + this.color + this.piece; 
	}
}

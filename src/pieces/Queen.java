/**
 * Queen chess piece, subclass of pieces
 *
 * @author Gloria Salas Paucar; netid: gjs143
 * @author Sandhya Veludandi ; netid: sv549
 */

package pieces;

import board.Tile;

public class Queen implements Pieces {
	
	private char color;
	private char piece = 'Q'; 

	public Queen(char c) {
		this.color = c; 
	}
	
	public char getColor() {
		return this.color; 
	}
	
	public boolean isLegalMove(Tile start, Tile end, Tile[][] chessBoard) {
		
//		Rows corresponds to Y
//		Cols corresponds to X
//		row change -> vertical change
//		col change -> horizontal change
		
		//end tile's piece is not the same team as start tile's piece
		if(end.getPiece() != null && start.getPiece().getColor() == end.getPiece().getColor()) return false; 
						
		//moving horizontally means the columns change
		int horizontalMoves = Math.abs(start.getC() - end.getC()); 
		//moving vertically means the rows change
		int verticalMoves = Math.abs(start.getR() - end.getR()); 
		
		//Rook's moves: (horizontal/vertical): 
		//moving horizontally
		if(horizontalMoves > 0 && verticalMoves == 0) {
			//moving left horizontally -> column decreases
			if(start.getC() > end.getC()) {
				for(int c = start.getC() - 1; c > end.getC(); c--) {
					//if there's a piece in the pathway from the start to end tile -> illegal move
					if(chessBoard[start.getR()][c].getPiece() != null) return false; 
				}
						
				return true; 
			}
			else {
			//moving right 
				for(int c = start.getC() + 1; c < end.getC(); c++) {
					//if there's a piece in the pathway from the start to end tile -> illegal move
					if(chessBoard[start.getR()][c].getPiece() != null) return false; 
				}
						
				return true; 
						
			}
		}
				
		//moving vertically
		if(horizontalMoves == 0 && verticalMoves > 0) {
			//moving up vertically -> row decreases
			if(start.getR() > end.getR()) {
				for(int r = start.getR() - 1; r > end.getR(); r--) {
					//if there's a piece in the pathway from the start to end tile -> illegal move
					if(chessBoard[r][start.getC()].getPiece() != null) return false; 
				}
								
				return true; 
			}
			else {
			//moving down
				for(int r = start.getR() + 1; r < end.getR(); r++) {
					//if there's a piece in the pathway from the start to end tile -> illegal move
					if(chessBoard[r][start.getC()].getPiece() != null) return false; 
				}
						
				return true; 
								
			}
		}
		
		//Bishop's moves: Diagonal moves: 
		//check if Queen moves in diagonal: 
		if(horizontalMoves != verticalMoves) return false; 
		
		//up: decreasing row, left: decreasing col
		if(start.getR() > end.getR() && start.getC() > end.getC()) {
			for(int r = start.getR() - 1, c = start.getC() - 1; 
				r > end.getR() && c > end.getC(); 
				r--, c--) {
					if(chessBoard[r][c].getPiece() != null) return false; 
			}
			return true; 
		}
		
		//up: decreasing row, right: increasing col
		if(start.getR() > end.getR() && start.getC() < end.getC()) {
			for(int r = start.getR() - 1, c = start.getC() + 1; 
				r > end.getR() && c < end.getC(); 
				r--, c++) {
					if(chessBoard[r][c].getPiece() != null) return false; 
			}
			return true; 
		}
		
		//down: increasing row, left: decreasing col
		if(start.getR() < end.getR() && start.getC() > end.getC()) {
			for(int r = start.getR() + 1, c = start.getC() - 1; 
				r < end.getR() && c > end.getC(); 
				r++, c--) {
					if(chessBoard[r][c].getPiece() != null) return false; 
			}
			return true; 
		}
		
		//down: increasing row, right: increasing col
		if(start.getR() < end.getR() && start.getC() < end.getC()) {
			for(int r = start.getR() + 1, c = start.getC() + 1; 
				r < end.getR() && c < end.getC(); 
				r++, c++) {
					if(chessBoard[r][c].getPiece() != null) return false; 
			}
					return true; 
		}
				
		
		return false; 
	}
	
	public String toString() {
		return "" + this.color + this.piece; 
	}
}


/**
 * Rook chess piece, subclass of pieces
 * 
 * @author Gloria Salas Paucar; netid: gjs143
 * @author Sandhya Veludandi ; netid: sv549
 */

package pieces;

import board.Tile;

public class Rook implements Pieces {
	
	private char color;
	private char piece = 'R'; 
	
	public boolean initialMove; 

	public Rook(char c) {
		this.color = c;
		initialMove = true; 
	}
	
	public char getColor() {
		return this.color; 
	}
	
	//is the move allowed for a Rook?
	public boolean isLegalMove(Tile start, Tile end, Tile[][] chessBoard) {
		
//		Rows corresponds to Y
//		Cols corresponds to X
//		row change -> vertical change
//		col change -> horizontal change
		
		//the rook can move vertically, horizontally, and cannot jump over its own pieces
		
		//end tile's piece is not the same team as start tile's piece
		if(end.getPiece() != null && start.getPiece().getColor() == end.getPiece().getColor()) return false; 
		
		//moving horizontally means the columns change
		int horizontalMoves = Math.abs(start.getC() - end.getC()); 
		//moving vertically means the rows change
		int verticalMoves = Math.abs(start.getR() - end.getR()); 
		
		//rook can't move horizontally AND vertically
		if(horizontalMoves > 0 && verticalMoves > 0) return false; 
		
		//moving horizontally
		if(horizontalMoves > 0 && verticalMoves == 0) {
			
			//moving left horizontally -> column decreases
			if(start.getC() > end.getC()) {
				for(int c = start.getC() - 1; c > end.getC(); c--) {
					//if there's a piece in the pathway from the start to end tile -> illegal move
					if(chessBoard[start.getR()][c].getPiece() != null) return false; 
				}
				initialMove = false; 
				return true; 
			}
			else {
				//moving right 
				for(int c = start.getC() + 1; c < end.getC(); c++) {
					//if there's a piece in the pathway from the start to end tile -> illegal move
					if(chessBoard[start.getR()][c].getPiece() != null) return false; 
				}
				initialMove = false; 
				return true; 
				
			}
		}
		
		//moving vertically
		if(horizontalMoves == 0 && verticalMoves > 0) {
			//moving up vertically -> row decreases
			if(start.getR() > end.getR()) {
				for(int r = start.getR() - 1; r > end.getR(); r--) {
					//if there's a piece in the pathway from the start to end tile -> illegal move
					if(chessBoard[r][start.getC()].getPiece() != null) return false; 
				}
				initialMove = false; 		
				return true; 
			}
			else {
				//moving down
				for(int r = start.getR() + 1; r < end.getR(); r++) {
					//if there's a piece in the pathway from the start to end tile -> illegal move
					if(chessBoard[r][start.getC()].getPiece() != null) return false; 
				}
				initialMove = false; 
				return true; 
						
			}
		}
				
		return false; 
	}
	
	public String toString() {
		return "" + this.color + this.piece; 
	}
}

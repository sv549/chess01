/**
 * The basis for a standard chess piece
 *
 * @author Gloria Salas Paucar; netid: gjs143
 * @author Sandhya Veludandi ; netid: sv549
 */

package pieces;

import board.Tile;

public interface Pieces {
	char getColor(); 
	boolean isLegalMove(Tile start, Tile end, Tile[][] chessBoard); 
	String toString(); 
}
/**
 * Used by each chess piece to validate if a move is legal
 *
 * @param start Where the piece is currently
 * @param end Where the piece wants to go
 * @param Tile The chess board
 * @return True if the piece can move ot the end point legally, false otherwise
 */
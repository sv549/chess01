/**
 * Bishop chess piece, subclass of pieces
 *
 * @author Gloria Salas Paucar; netid: gjs143
 * @author Sandhya Veludandi ; netid: sv549
 */

package pieces;

import board.Tile;

public class Bishop implements Pieces {
	
	private char color;
	private char piece = 'B'; 

	public Bishop(char c) {
		this.color = c; 
	}
	
	public char getColor() {
		return this.color; 
	}
	
	public boolean isLegalMove(Tile start, Tile end, Tile[][] chessBoard) {
		
//		Rows corresponds to Y
//		Cols corresponds to X
//		row change -> vertical change
//		col change -> horizontal change
		
		//end tile's piece is not the same team as start tile's piece
		if(end.getPiece() != null && start.getPiece().getColor() == end.getPiece().getColor()) return false; 
				
		//moving horizontally means the columns change
		int horizontalMoves = Math.abs(start.getC() - end.getC()); 
		//moving vertically means the rows change
		int verticalMoves = Math.abs(start.getR() - end.getR()); 
		
		//check if bishop moves in diagonal: 
		if(horizontalMoves != verticalMoves) return false; 
		
		//up: decreasing row, left: decreasing col
		if(start.getR() > end.getR() && start.getC() > end.getC()) {
			for(int r = start.getR() - 1, c = start.getC() - 1; 
				r > end.getR() && c > end.getC(); 
				r--, c--) {
					if(chessBoard[r][c].getPiece() != null) return false; 
			}
			return true; 
		}
		
		//up: decreasing row, right: increasing col
		if(start.getR() > end.getR() && start.getC() < end.getC()) {
			for(int r = start.getR() - 1, c = start.getC() + 1; 
				r > end.getR() && c < end.getC(); 
				r--, c++) {
					if(chessBoard[r][c].getPiece() != null) return false; 
			}
			return true; 
		}
		
		//down: increasing row, left: decreasing col
		if(start.getR() < end.getR() && start.getC() > end.getC()) {
			for(int r = start.getR() + 1, c = start.getC() - 1; 
				r < end.getR() && c > end.getC(); 
				r++, c--) {
					if(chessBoard[r][c].getPiece() != null) return false; 
			}
			return true; 
		}
		
		//down: increasing row, right: increasing col
		if(start.getR() < end.getR() && start.getC() < end.getC()) {
			for(int r = start.getR() + 1, c = start.getC() + 1; 
				r < end.getR() && c < end.getC(); 
				r++, c++) {
					if(chessBoard[r][c].getPiece() != null) return false; 
			}
					return true; 
		}
				
		return false; 
	}
	
	public String toString() {
		return "" + this.color + this.piece; 
	}
}

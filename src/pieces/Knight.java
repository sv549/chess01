/**
 * Knight chess piece, subclass of pieces
 * 
 * @author Gloria Salas Paucar; netid: gjs143
 * @author Sandhya Veludandi ; netid: sv549
 */


package pieces;

import board.Tile;

public class Knight implements Pieces {
	
	private char color;
	private char piece = 'N'; 

	public Knight(char c) {
		this.color = c; 
	}
	
	public char getColor() {
		return this.color; 
	}
	
	public boolean isLegalMove(Tile start, Tile end, Tile[][] chessBoard) {
		
//		Rows corresponds to Y
//		Cols corresponds to X
//		row change -> vertical change
//		col change -> horizontal change
		
		//end tile's piece is not the same team as start tile's piece
		if(end.getPiece() != null && start.getPiece().getColor() == end.getPiece().getColor()) return false; 
		
		//moving horizontally means the columns change
		int horizontalMoves = Math.abs(start.getC() - end.getC()); 
		//moving vertically means the rows change
		int verticalMoves = Math.abs(start.getR() - end.getR()); 
		
		//left/right 2, up/down 1
		if(horizontalMoves == 2 && verticalMoves == 1) return true; 
		
		//left/right 1, up/down 2
		if(horizontalMoves == 1 && verticalMoves == 2) return true;
		
		return false; 
	}
	
	public String toString() {
		return "" + this.color + this.piece; 
	}
}

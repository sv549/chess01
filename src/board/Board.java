//@ Gloria Salas netid: gjs143
//@ Sandhya Veludandi ; netid: sv549

package board;
import pieces.*; 




public class Board {
	private static char letters[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'}; 
	private static int num[] = {8, 7, 6, 5, 4, 3, 2, 1}; 
	public static Tile chessBoard[][];
	
	public Board() {
		chessBoard = new Tile[8][8]; 
		createPiecesTiles(); 
	}
	
    
	private void createBlackPieces() {
		  //rows 0, 1 contain the black pieces 
		chessBoard[0][0] = new Tile(0, 0, new Rook('b'));
		chessBoard[0][1] = new Tile(0, 1, new Knight('b')); 
		chessBoard[0][2] = new Tile(0, 2, new Bishop('b')); 
		chessBoard[0][3] = new Tile(0, 3, new Queen('b')); 
		chessBoard[0][4] = new Tile(0, 4, new King('b')); 
		chessBoard[0][5] = new Tile(0, 5, new Bishop('b')); 
		chessBoard[0][6] = new Tile(0, 6, new Knight('b'));
		chessBoard[0][7] = new Tile(0, 7, new Rook('b'));
		for(int c = 0; c < 8; c++) {
			chessBoard[1][c] = new Tile(1, c, new Pawn('b')); 
		}
	}

	private void createWhitePieces() {
		//rows 6, 7 contain the white pieces
		for(int c = 0; c < 8; c++) {
			chessBoard[6][c] = new Tile(6, c, new Pawn('w')); 
		}
		chessBoard[7][0] = new Tile(7, 0, new Rook('w'));
		chessBoard[7][1] = new Tile(7, 1, new Knight('w'));
		chessBoard[7][2] = new Tile(7, 2, new Bishop('w'));
		chessBoard[7][3] = new Tile(7, 3, new Queen('w'));
		chessBoard[7][4] = new Tile(7, 4, new King('w'));
		chessBoard[7][5] = new Tile(7, 5, new Bishop('w'));
		chessBoard[7][6] = new Tile(7, 6, new Knight('w'));
		chessBoard[7][7] = new Tile(7, 7, new Rook('w'));		
	}
	
	private void createEmptyTiles() {
		for(int r = 2; r < 6; r++) {
			for(int c = 0; c < 8; c++) {
				chessBoard[r][c] = new Tile(r, c, null); 
			}
		}
	}
	
	public void createPiecesTiles() {
		createBlackPieces(); 
		createEmptyTiles(); 
		createWhitePieces(); 
	
	}
	
	public void printTiles() {
		System.out.println(""); 
		//loops through the chessBoard to print out each tile
		for(int r = 0; r < chessBoard.length; r++) {
			for(int c = 0; c < chessBoard.length; c++) {
				//check if tile contains a chess Piece
				if(chessBoard[r][c].getPiece() != null) {
					//if it does, print out the chess piece
					System.out.print(chessBoard[r][c].getPiece() + " ");
				}
				else {
					//otherwise print out the string color representation of the tile
					if (chessBoard[r][c].getTileColor() == 1) {
						System.out.print("## "); 
					}
					else {
						System.out.print("   "); 
					}
				}
			} 
			System.out.print(num[r]);
			System.out.println(""); 
		}
		System.out.print(" ");
	}

	
	//prints out a-h on the bottom of the board
	private void printLetters() {
		for(int i = 0; i < letters.length; i++) {
				System.out.print(letters[i] + "  "); 
		}
		System.out.println(""); 
		System.out.println(""); 
	}
	
	public void printBoard() {
		printTiles(); 
		printLetters(); 
	}
	
	public Tile parseInput(String input) {
		String letter = input.substring(0, 1); 
		String number = input.substring(1, 2); 
		int r = 8 - Integer.parseInt(number); 
		int c = charAt(letter); 
		//System.out.println(r + " " + c); 
		return chessBoard[r][c]; 
	}

	//method to get any number of first characters of a string
	private int charAt(String myStr) {
		int result = myStr.charAt(0)-97;
		return result;
	}
	
	//check if move is legal
	public boolean move(String src, boolean isWhite) {
        //Invalid input
        if(src.length() < 5) return false;

        //create the starting and ending tiles
        Tile start = parseInput(src.substring(0,2));
        Tile end = parseInput(src.substring(3,5));
        
        Pieces startPiece = start.getPiece(); 

        //checks if the start tile's chess piece exists
        if(startPiece == null) return false;

        
        //checks if the player is allowed to move chess piece on start tile
        //meaning that checks if chess piece is same color as the player's
        boolean white = (startPiece.getColor() == 'w') ? true : false;
        
        //Check to make sure player is moving their own piece
        if(white != isWhite) return false; 

        //Check input included a promotion character
        if(src.length() >= 7 && (startPiece instanceof Pawn)) {
           Pawn p = (Pawn) startPiece; 
           char promotionChar = src.charAt(6); 
           p.setPromotionChar(promotionChar);
           startPiece = p; 
        }

        //Check to make sure the specific piece can legally move from start to end
        if(!startPiece.isLegalMove(start, end, chessBoard)) return false;

//        //Player executed en passant
//        if(startTile.piece.enpassant == '1') {
//            chessBoard[start.getY()][end.getX()] = null;
//        }
        
        
        //Player executed castling
        if(startPiece instanceof King) {

        	if(((King) startPiece).castling == 'l') {
	        	//move castle
	            chessBoard[end.getR()][end.getC() + 1].setPiece(chessBoard[end.getR()][0].getPiece());
	            chessBoard[end.getR()][0].setPiece(null);
        	}
        	else if(((King) startPiece).castling == 'r') {
        		
	        	//move the castle
	            chessBoard[end.getR()][end.getC() - 1].setPiece(chessBoard[end.getR()][7].getPiece());
	            chessBoard[end.getR()][7].setPiece(null);
        	}
        }

    
        //Move is legal -> move chess piece from start to end tile
        end.setPiece(startPiece); //so set end tile's chess piece to start tile's chess piece
        start.setPiece(null);//set start tile's chess piece to null (bc it moved to end tile so now start is empty)
        
        return true;
		
	}
}

//@ Gloria Salas netid: gjs143
//@ Sandhya Veludandi ; netid: sv549

package board;

import pieces.Pieces;

public class Tile {
	
	public Pieces piece;
	private int r;
	private int c;
	
	public Tile(int r, int c, Pieces piece)
	{
		this.setPiece(piece);
	    this.r = r; 
	    this.c = c; 
	}
	
	public Pieces getPiece()
	{
		return this.piece;
	}
	
	public void setPiece(Pieces p)
	{
		this.piece = p;
	
	}
	
	public int getR()
	{
		return this.r;
	}
	
	public int getC()
	{
		return this.c;
	}

	
	//black is 1, white is 0
	public int getTileColor() {
		if (r % 2 == 0) {
			if(c % 2 == 1) return 1; 
			else return 0; 
		}
		else {
			if(c % 2 == 0) return 1; 
			else return 0; 
		}
	}
}

